import { FC } from 'react'
import { Metadata, ResolvingMetadata } from 'next'

import { Article } from '@components/Blog/Shared/Article/Article'
import { generateBaseMetadata } from 'app/getBaseMetadata'
import { fetchBlogContent } from '@cms/fetchers/blogFetcher'

export const revalidate = 600
export const dynamic = 'force-static'

type Props = {
	params: Promise<{ slug: string }>
	searchParams: Promise<{ [key: string]: string | string[] | undefined }>
}

export async function generateStaticParams() {
	return []
}

export async function generateMetadata(
	{ params, searchParams }: Props,
	parent: ResolvingMetadata
): Promise<Metadata> {
	const { slug } = await params

	const post = await fetchBlogContent(slug)

	return {
		...generateBaseMetadata({ urlPath: `blog/${slug}` }),
		title: post.title,
	}
}

const BlogPost: FC<Props> = async ({ params, searchParams }) => {
	const [{ slug }, { data }] = await Promise.all([params, searchParams])

	const post = await fetchBlogContent(slug)
	return <Article {...post} />
}

export default BlogPost
