import { Metadata } from 'next'

import { FC } from 'react'

import { Home } from '@components/Blog/Home/Home'
import { generateBaseMetadata, baseViewport } from 'app/getBaseMetadata'

export const revalidate = 600
// export const dynamic = 'force-static'

export const metadata: Metadata = {
	...generateBaseMetadata({ urlPath: 'blog' }),
	title: '/blog | jc',
}

export const viewport = baseViewport

export async function generateStaticParams() {
	return []
}

type Props = {
	params: Promise<{ slug: string }>
	searchParams: Promise<{ [key: string]: string | string[] | undefined }>
}

const Page: FC<Props> = async ({ searchParams }) => {
	const queries = await searchParams

	const showDraft = queries['showDraft'] !== undefined

	return <Home showDraft={showDraft} />
}

export default Page
