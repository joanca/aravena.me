import { FC } from 'react'
import { Metadata, ResolvingMetadata } from 'next'

import { generateBaseMetadata } from 'app/getBaseMetadata'
import { PostPreview, PostPreviewProps } from '@components/Admin/Publish/PostPreview'
import { decodeFromBase64 } from 'src/modules/encodeData'

type Props = {
	params: Promise<{ slug: string }>
	searchParams: Promise<{ [key: string]: string | string[] | undefined }>
}

export async function generateMetadata(
	{ params, searchParams }: Props,
	parent: ResolvingMetadata
): Promise<Metadata> {
	const [{ slug }, { data }] = await Promise.all([params, searchParams])

	const decodedData = getDecodedParams(data as string)
	return {
		...generateBaseMetadata({ urlPath: `blog/${decodedData.slug}` }),
		title: decodedData.title,
		robots: 'noindex, nofollow',
	}
}

const BlogPost: FC<Props> = async ({ params, searchParams }) => {
	const [{ slug }, { data }] = await Promise.all([params, searchParams])

	const decodedData = getDecodedParams(data as string)

	return <PostPreview {...decodedData} />
}

const getDecodedParams = (b64data: string): PostPreviewProps => {
	const decodedDataParamsString = decodeFromBase64(b64data)
	const parsedParams = JSON.parse(decodedDataParamsString) as Record<
	string,
	string
	>

	const decodedData = Object.fromEntries(new URLSearchParams(parsedParams))

	return decodedData as unknown as PostPreviewProps
}

export default BlogPost
