import { Metadata } from 'next'

import { Reading } from '@components/Reading/Reading'
import { generateBaseMetadata } from 'app/getBaseMetadata'

export const revalidate = 600
export const dynamic = 'force-static'

export const metadata: Metadata = {
	...generateBaseMetadata({ urlPath: 'reading' }),
	title: 'My reading list',
	description:
    'These are the article that I find important and that I will read later, come and read them with me.',
}

export default Reading
