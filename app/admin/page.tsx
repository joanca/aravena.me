import { fetchCustomCollections } from '@cms/fetchers/customCollectionFetcher'
import { CMSWrapper } from '@components/Admin/Admin'
import { getDecapCMSConfig } from 'src/modules/admin/config'

const Page = async () => {
	const customCollections = await fetchCustomCollections()
	const cmsConfig = getDecapCMSConfig({ collections: customCollections })

	return <CMSWrapper cmsConfig={cmsConfig} />
}

export default Page
