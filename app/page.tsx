import { Metadata } from 'next'

import { generateBaseMetadata, baseViewport } from './getBaseMetadata'

import { Home } from 'src/components/Home/Home'

export const metadata: Metadata = {
	...generateBaseMetadata({ urlPath: '' }),
	title: 'Juan Carlos Aravena Esparza',
	description: `I'm a Software Engineer with a focus on frontend, taking special'care of accessibility.
  In the past I've been at Thoughtworks, Platzi and Evernote.`,
}

export const viewport = baseViewport

export default Home