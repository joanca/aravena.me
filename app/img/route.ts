import { NextApiHandler } from 'next';
import { NextResponse } from 'next/server';
import sharp from 'sharp';

import { getRawFileUrl } from 'src/clients/gitlabClient';

export const GET: NextApiHandler = async (req) => {
	const imageUrl = new URL(req.url as string)
	const { searchParams } = imageUrl
	const width = searchParams.get('w')
	const height = searchParams.get('h')
	const quality = searchParams.get('q')

	const widthInt = width ? parseInt(width, 10) : null;
	const heightInt = height ? parseInt(height, 10) : null;
	const qualityInt = quality ? parseInt(quality, 10) : 100;
	
	const src = isExternalImage(imageUrl)
		? searchParams.get('src') as string
		: getSrcFromPathname(imageUrl)
	

	if (!src || Array.isArray(src)) {
		return NextResponse.json("Invalid image source", { status: 400 });
	}

	const imageData = await fetch(parseImageUrl(src));
	const imageBuffer = await imageData.arrayBuffer()

	const headers = new Headers({
		'Cache-Control': 'max-age: 31536000, immutable',
		'Content-Type': 'image/webp'
	})

	try {
		const resizedImageBuffer = await sharp(imageBuffer, { failOn: 'none' })
			.toFormat('webp', { quality: qualityInt })
			.resize(widthInt, heightInt)
			.toBuffer()

		return new NextResponse(resizedImageBuffer, { status: 200, statusText: "OK", headers });
	} catch (error) {
		console.error(error)

		return new NextResponse(imageBuffer, { status: 200, statusText: "OK", headers });
	}

}

const parseImageUrl = (src: string) => {
	if(!src.includes('http')) return getRawFileUrl(src)

	if(src.includes('https://gitlab.com')) {
		const { pathname } = new URL(src)
  
		const filePath = pathname.split('/files/')[1].split('/raw')[0]
  
		return getRawFileUrl(filePath)
	}

	return src
}

export const isExternalImage = (url: URL): boolean => url.pathname === "/img"

const getSrcFromPathname = (url: URL): string => {
	const { pathname } = url

	return pathname.split('/img/')[1]
}