import { SpeedInsights } from "@vercel/speed-insights/next"

import { FCC } from '@components/types';

import 'src/components/globals.css';

const RootLayout: FCC = ({ children }) => {
	return (
		<html lang="en">
			<body>
				{children}
				<SpeedInsights />
			</body>
		</html>
	)
}

export default RootLayout
