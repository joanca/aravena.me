import { Metadata, Viewport } from "next";

import { DOMAIN } from "src/env";

type BaseMetadata = {
	urlPath?: string
}

export const generateBaseMetadata = ({ urlPath }: BaseMetadata): Metadata => ({
	robots: 'index, follow',
	alternates: {
		canonical: getCanonicalUrl(urlPath)
	}
})

export const baseViewport: Viewport =  {
	width: 'device-width',
	initialScale: 1,

}	

export const getCanonicalUrl = (urlPath = '') => {
	if(!urlPath) return DOMAIN

	return `${DOMAIN}/${urlPath}`
}