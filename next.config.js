// @ts-check
 
const IS_DEV = process.env.NODE_ENV === "development"

const prodConfig = {
	eslint: {
		ignoreDuringBuilds: true,
	},
	typescript: {
		ignoreBuildErrors: true,
	  },
}

const baseConfig = {
	reactStrictMode: true,
	experimental: {
		mdxRs: true,
	},
	transpilePackages: ['@uiw/react-md-editor', '@uiw/react-markdown-preview'],
	images: {
		domains: ['static.aravena.me', 'blogaravena.files.wordpress.com'],
	},
	async redirects() {
		return [
			{
				source: '/blog/page/1',
				destination: '/blog',
				permanent: false
			},
			{
				source: '/blog/page/0',
				destination: '/blog',
				permanent: false
			},
		];
	},
}

/**
 * @type {import('next').NextConfig}
 **/
module.exports = IS_DEV ? { ...baseConfig } : { ...baseConfig, ...prodConfig }
