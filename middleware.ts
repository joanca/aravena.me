import {
  NextFetchEvent,
  NextMiddleware,
  NextRequest,
  NextResponse,
} from 'next/server'

import { analyticsMiddleware } from 'src/middleware/analyticsMiddleware'

export type MiddlewareProps = {
  request: NextRequest
  response: NextResponse
  event: NextFetchEvent
}

export type Middleware = (args: MiddlewareProps) => void

export const middleware: NextMiddleware = async (request, event) => {
  const url = request.nextUrl.clone()

  if (excludedUrl(url.pathname)) return NextResponse.next()

  const response = NextResponse.next({
    headers: {
      'Cache-Control': 'public, s-maxage=600, stale-while-revalidate=3600',
    },
  })

  const middlewareProps: MiddlewareProps = { request, event, response }

  analyticsMiddleware(middlewareProps)

  return response
}

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - favicon.ico (favicon file)
     */
    '/((?!api|img|sitemap|_next/static|favicon.ico).*)',
  ],
}

const excludedUrl = (pathname: string): boolean => {
  const EXCLUDED_PATHS = ['static']

  const basePath = pathname.split('/')[1]

  return EXCLUDED_PATHS.includes(basePath)
}
