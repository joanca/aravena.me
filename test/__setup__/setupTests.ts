/// <reference types="@testing-library/jest-dom" />
import 'test/__setup__/env'
import { server } from 'test/__mocks__/server'
import '@testing-library/jest-dom'

import 'whatwg-fetch'

beforeAll(() => server.listen())

afterEach(() => server.resetHandlers())

afterAll(() => server.close())
