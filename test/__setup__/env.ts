const HOST_ENV = {
	VERCEL_ENV: 'development',
}

const CMS_ENV = {
	WORDPRESS_API_V2_URL: 'https://public-api.wordpress.com/wp/v2/sites',
	WORDPRESS_BLOG_URL: 'blog.wp.com',
	WORDPRESS_ACCESS_TOKEN_B64: 'dG9rZW4=', // token
}

export const ENV: NodeJS.ProcessEnv = {
	NODE_ENV: 'test',
	NEXT_PUBLIC_STATIC_PROXY_URL: 'https://static.proxy',
	NEXT_PUBLIC_DOMAIN: 'https://localhost',
	REVALIDATE_PAGES: '10',
	...CMS_ENV,
	...HOST_ENV,
}

// replace node env with ENV
process.env = ENV
