import { Article } from 'src/clients/types'

export const getSortedArticles: Article[] = [
	{
		// eslint-disable-next-line max-len
		excerpt:
      'Our political opinions and attitudes are an important part of who we are and how we construct our identities. Hence, if I ask your opinion on health care, you will not only share it with me, but you will likely resist any of my attempts to persuade you of another point of view.',
		id: 2780948231,
		title: 'How Political Opinions Change',
		url: 'https://getpocket.com/explore/item/how-political-opinions-change',
		wordCount: 1113,
		readTime: 5,
		image: 'https://getpocket.com/image.jpg',
	},
	{
		// eslint-disable-next-line max-len
		excerpt:
      'El caso de Colombia es muy curioso. Ningún país latinoamericano ha padecido tantas guerras civiles y, sin embargo, con la misma seguridad puede decirse que ningún otro ha sido más libre, civil y democrático en ese mismo período.',
		id: 3262647005,
		title: 'Columna de Mario Vargas Llosa: El ejemplo colombiano',
		// eslint-disable-next-line max-len
		url: 'https://www.latercera.com/opinion/noticia/columna-de-mario-vargas-llosa-el-ejemplo-colombiano/PCSTMFCSZVB6VLTRQUU6QGQP7M/',
		wordCount: 1319,
		readTime: 6,
		image: 'https://getpocket.com/image.jpg',
	},
	{
		// eslint-disable-next-line max-len
		excerpt:
      'Jessica Riskin teaches History at Stanford. Her latest book is The Restless Clock: A History of the Centuries-Long Argument Over What Makes Living Things Tick. (March 2021)',
		id: 3260197491,
		title: 'When Engineers Were Humanists',
		url: 'https://www.nybooks.com/articles/2021/03/11/engineers-humanists-renaissance-inventions/',
		wordCount: 28,
		readTime: null,
		image: 'https://getpocket.com/image.jpg',
	},
	{
		excerpt: '10991',
		id: 3251674722,
		title: 'Presidente Piñera defiende control de identidad preventivo',
		// eslint-disable-next-line max-len
		url: 'https://cooperativa.cl/noticias/pais/region-de-los-rios/presidente-pinera-defiende-control-de-identidad-preventivo/2021-02-08/120048.html',
		wordCount: 0,
		readTime: null,
		image: 'https://getpocket.com/image.jpg',
	},
]
