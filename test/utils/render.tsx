import { renderHook as renderHookOriginal } from '@testing-library/react-hooks'
import React, { ReactNode } from 'react'
import { SWRConfig } from 'swr'

type RenderHookType = typeof renderHookOriginal

type Children = {
	children: ReactNode
}

export const renderHook: RenderHookType = (callback) => {
	const wrapper = ({ children }: Children) => (
		<SWRConfig value={{ provider: () => new Map() }}>{children}</SWRConfig>
	)

	// @ts-expect-error wrapper has an incompatible type, but it works
	return renderHookOriginal(callback, { wrapper })
}
