import { rest } from 'msw'

import pocketArticles from 'test/__mocks__/pocket/response/allArticles.json'

export const handlers = [
  rest.post(new RegExp('https://getpocket.com/v3/get'), (req, res, ctx) =>
    res(
      // Respond with a 200 status code

      ctx.status(200),
      ctx.json(pocketArticles)
    )
  ),
]
