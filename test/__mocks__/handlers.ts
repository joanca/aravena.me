import { RestHandler, MockedRequest, DefaultRequestBody } from 'msw'

import { handlers as pocketHandlers } from 'test/__mocks__/pocket/handlers'

export const handlers: RestHandler<MockedRequest<DefaultRequestBody>>[] = [
  ...pocketHandlers,
]
