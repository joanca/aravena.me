import { imageLoader } from 'src/modules/imageResizer'

describe('imageResizer module', () => {
  const IMAGE_URL = 'https://example.com/path/to/image.jpg'

  describe('imageLoader', () => {
    it('should return image service url with default quality and specified width', () => {
      const encodedImageUrl = encodeURIComponent(IMAGE_URL)
      const expectedUrl = `/img?src=${encodedImageUrl}&w=100&q=75`

      expect(imageLoader(IMAGE_URL, { width: 100 })).toBe(expectedUrl)
    })
  })
})
