module.exports = {
	darkMode: 'media', // or 'media' or 'class'
	content: [
		"./app/**/*.{js,ts,jsx,tsx,mdx}",
		"./pages/**/*.{js,ts,jsx,tsx,mdx}",

		// Or if using `src` directory:
		"./src/**/*.{js,ts,jsx,tsx,mdx}",
	],
	theme: {
		container: {
			padding: '1rem',
		},
		maxWidth: {
			'1/4': '25%',
			'1/2': '50%',
			'3/4': '75%',
			full: '100%',
			min: 'min-content',
			max: 'max-content',
		},
		minWidth: {
			'1/4': '25%',
			'1/2': '50%',
			'3/4': '75%',
			full: '100%',
			min: 'min-content',
			max: 'max-content',
		},
	},
	variants: {
		extend: {},
	},
	plugins: [],
}
