/* eslint-disable */
/* eslint-disable @typescript-eslint/no-var-requires */
// @ts-ignore config by vercel for nextjs
// https://nextjs.org/docs/advanced-features/compiler#jest
// jest.config.js
const nextJest = require('next/jest')

// Providing the path to your Next.js app which will enable loading next.config.js and .env files
const createJestConfig = nextJest({ dir: './' })

// Any custom config you want to pass to Jest
const customJestConfig = {
  rootDir: '.',
  setupFilesAfterEnv: [
    '@testing-library/jest-dom',
    '<rootDir>/test/__setup__/setupTests.ts',
  ],
  moduleDirectories: ['node_modules', '<rootDir>/node_modules', '.'],
  testEnvironment: '<rootDir>/test/__setup__/jest.environment.cjs',
}

// createJestConfig is exported in this way to ensure that next/jest can load the Next.js configuration, which is async
module.exports = createJestConfig(customJestConfig)
