---
status: published
title: 0AD HiDPI configuration
slug: 0ad-hidpi-configuration
publish_date: 2018-02-14T18:26:39.062Z
---
I'm a fan of RTS games and 0AD has been one of those games that I wanted to try for a couple of years now, so I installed it
on my new laptop, a Dell XPS 13 with a 4K (retina) display. The game works like a charm, of course, but the GUI is not
configured by default to work on a HiDPI screen.

I was googling a bit and came to a [thread in the forums](https://wildfiregames.com/forum/index.php?/topic/23990-unreadable-text-buttons-too-small-to-be-clickable-hidpi-yada-yada/ "Wildfire forums"), posted a few days ago, and found out that there's an option that scales the game. Add this line to your `user.cfg` file.

```YAML
gui.scale = "2.0"
```

If you don't know where this file is, look at [this page](https://trac.wildfiregames.com/wiki/GameDataPaths). There's info for Windows, Linux and OS X.

Go on a try it, the GUI looks a bit pixelated, but nothing really annoying.
