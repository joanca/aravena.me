---
status: published
title: Rebuilding this blog again
slug: rebuilding-this-blog-again
publish_date: 2023-12-04T20:39:34.335Z
featured_image: /public/static/uploads/decap-logo.svg
---
It's that time again when I decide to rebuild this blog with the idea of testing new technologies and hopefully start writing consistently, which has not happened really.

This time I'm excited about the way the blog is built, using [MDX](https://mdxjs.com/) and [React Server Components](https://react.dev/reference/react-dom/server), using [Decap CMS](https://decapcms.org/) and my own git repository as the backend for the content. I love this setup.

I'll try to write more and possibly explain how I'm building this, the custom preview component that I use and how to leverage the Gitlab API to fetch content on real time.

See you soon,

JC
