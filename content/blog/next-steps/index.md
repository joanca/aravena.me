---
status: published
title: Next steps
slug: next-steps
publish_date: 2025-01-09T19:23:00.000Z
---
2025 will be an interesting year.

I'm moving right now to a new apartment in Amsterdam, yes, I live here since last year.

I started a new job at the end of last year.

I bought a boat last year, its an old sailing boat, but its big and we have parties on it around the canals in Amsterdam.

What is going to happen this year?
