// general
const LOCALHOST_URL = `http://localhost:3000`
export const IS_SERVER =
  typeof process === 'object' && String(process) === '[object process]'
export const NODE_ENV = process.env.NODE_ENV
export const IS_PROD = NODE_ENV === 'production'
export const VERCEL_ENV = process.env.NEXT_PUBLIC_VERCEL_ENV as string
export const VERCEL_URL = `https://${
	process.env.NEXT_PUBLIC_VERCEL_URL as string
}`
export const DOMAIN =
  (NODE_ENV === 'development' && LOCALHOST_URL) ||
  (VERCEL_ENV === 'preview' && VERCEL_URL) ||
  (process.env.NEXT_PUBLIC_DOMAIN as string)
export const REVALIDATE_PAGES = 1800
export const GOOGLE_ANALYTICS = process.env.GOOGLE_ANALYTICS as string
export const BFF_URL = `${DOMAIN}/api`

// static assets
export const PUBLIC_STATIC_ASSETS_URL = `${DOMAIN}/${
	process.env.NEXT_PUBLIC_STATIC_ASSETS_PATH as string
}`
export const PUBLIC_STATIC_PROXY_URL = process.env
	.NEXT_PUBLIC_STATIC_PROXY_URL as string

// admin
export const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD as string

// Pocket
export const POCKET_ACCESS_TOKEN = process.env.POCKET_ACCESS_TOKEN as string
export const POCKET_CONSUMER_KEY = process.env.POCKET_CONSUMER_KEY as string

// Blog
export const POSTS_PER_PAGE = parseInt(process.env.POSTS_PER_PAGE as string, 10)
export const GITLAB_TOKEN = process.env.GITLAB_TOKEN as string
export const GITLAB_PROJECT_ID = process.env
	.NEXT_PUBLIC_GITLAB_PROJECT_ID as string
export const GITLAB_BRANCH_REF = process.env.VERCEL_GIT_COMMIT_REF || 'master'
