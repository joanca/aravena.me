import { Article, PocketArticle, PocketRawRequest } from './types'

const POCKET_CONSUMER_KEY = process.env.POCKET_CONSUMER_KEY as string
const POCKET_ACCESS_TOKEN = process.env.POCKET_ACCESS_TOKEN as string
const POCKET_API_RETRIEVE = 'https://getpocket.com/v3/get'

const getSortedArticles = async (): Promise<Article[]> => {
	const articles = await fetchArticles()

	const sortedArticles = articles.sort((a, b) => a.sort_id - b.sort_id)

	return sortedArticles.map(parseArticle)
}

const fetchArticles = async (): Promise<PocketArticle[]> => {
	const payload = {
		consumer_key: POCKET_CONSUMER_KEY,
		access_token: POCKET_ACCESS_TOKEN,
		count: 30,
		detailType: 'complete',
	}

	const request = await fetch(POCKET_API_RETRIEVE, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(payload),
	})

	const result = (await request.json()) as PocketRawRequest

	return Object.values(result.list)
}

const parseArticle = (article: PocketArticle): Article => ({
	id: parseInt(article.item_id, 10),
	title: article.resolved_title,
	url: article.resolved_url,
	excerpt: article.excerpt,
	wordCount: parseInt(article.word_count, 10),
	readTime: article.time_to_read ? article.time_to_read : null,
	image: article.top_image_url || null,
})

export { getSortedArticles }
