import { RepositoryCommit, RepositoryTree } from './types'

import { GITLAB_BRANCH_REF, GITLAB_PROJECT_ID, GITLAB_TOKEN } from 'src/env'

const API_BASE_URL = `https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}`

const gitlabClient = async (url: string): Promise<Response> => {
	return fetch(url, {
		headers: {
			Authorization: `Bearer ${GITLAB_TOKEN}`,
		},
	})
}

const slugWithExtension = (slug: string) => {
	if (slug.includes('.md')) return slug

	return `${slug}.md`
}

const isEmptyCollection = (data: unknown[] | { message: string }) => {
	return !Array.isArray(data) && Boolean(data?.message)
}

export const getRawContent = async (slug: string): Promise<string> => {
	const filePath = slugWithExtension(slug)
	const url = getRawFileUrl(filePath)

	const response = await gitlabClient(url)

	return response.text()
}

export const getRawFileUrl = (slug: string): string => {
	return `${API_BASE_URL}/repository/files/${encodeURIComponent(
		slug
	)}/raw?ref=${GITLAB_BRANCH_REF}`
}

export const getCollectionContent = async (
	collection: string
): Promise<RepositoryTree[]> => {
	const path = encodeURIComponent(collection)
	const url = `${API_BASE_URL}/repository/tree?path=${path}&ref=${GITLAB_BRANCH_REF}&recursive=true`

	const response = await gitlabClient(url)

	const data = (await response.json()) as RepositoryTree[]

	if (isEmptyCollection(data)) return []

	return data
}

export const getCommitsByPath = async (
	slug: string
): Promise<RepositoryCommit[]> => {
	const filePath = encodeURIComponent(`content/${slugWithExtension(slug)}`)
	const url = `${API_BASE_URL}/repository/commits?path=${filePath}&ref=${GITLAB_BRANCH_REF}&all=true`

	const response = await gitlabClient(url)

	return response.json() as unknown as RepositoryCommit[]
}

export const getLastCommitByPath = async (
	slug: string
): Promise<RepositoryCommit> => {
	const filePath = encodeURIComponent(slugWithExtension(slug))
	const url = `${API_BASE_URL}/repository/commits?path=${filePath}&ref_name=${GITLAB_BRANCH_REF}&per_page=1`

	const response = await gitlabClient(url)

	const data = (await response.json()) as unknown as RepositoryCommit[]

	return data[0]
}
