export type Article = {
	id: number
	title: string
	url: string
	excerpt: string
	wordCount: number
	readTime: number | null
	image: string | null
}

export type PocketRawRequest = {
	list: PocketArticle[]
}

export type PocketArticle = {
	item_id: string
	resolved_title: string
	resolved_url: string
	excerpt: string
	word_count: string
	time_to_read: number
	top_image_url: string
	domain_metadata: PocketDomainMetadata | null
	sort_id: number
}

type PocketDomainMetadata = {
	name: string
	logo: string
	greyscale_logo: string
}

export type RepositoryTree = {
	id: string;
	name: string;
	type: string;
	path: string;
	mode: string;
}

export type RepositoryCommit = {
	id: string,
	short_id: string,
	created_at: string,
	parent_ids: string[],
	title: string,
	message: string,
	author_name: string,
	author_email: string,
	authored_date: string,
	committer_name: string,
	committer_email: string,
	committed_date: string,
	web_url: url
}