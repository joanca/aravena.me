import { NextRequest, NextResponse, userAgent } from 'next/server'

import { GOOGLE_ANALYTICS, IS_PROD } from 'src/env'

export const ANALYTICS_COOKIE_NAME = 'client_id'

type AnalyticsPayloadType = {
	request: NextRequest
	screenSize?: string
	clientId: string
}

type AnalyticsPayload = {
	v: number
	tid: string
	cid: string
	ua: string
	t: string
	dt: string | undefined
	dl: string
	dr: string | null
	geoid: string
	ul: string | undefined
	sr: string | undefined
	ds: string
	aip: number
	z: number
}

export const buildPayload = async (
	data: AnalyticsPayloadType
): Promise<AnalyticsPayload> => {
	const { request, screenSize, clientId } = data
	const ua = userAgent(request)

	const { headers } = request

	const cid = clientId
	const acceptLanguage = headers.get('accept-language')
	const language = acceptLanguage ? acceptLanguage.split(',')[0] : undefined

	const payload = {
		v: 1,
		tid: GOOGLE_ANALYTICS,
		cid,
		ua: ua?.ua,
		t: 'pageview',
		dt: await getPageTitle(request),
		dl: request.url,
		dr: headers.get('referer'),
		geoid: headers.get('CF-IPCountry') || 'XX',
		ul: language,
		sr: screenSize,
		ds: 'web',
		aip: 1, // anonymizes IP
		z: Math.random(),
	}

	return payload
}

export const sendPageview = async (
	request: NextRequest,
	clientId: string
): Promise<void> => {
	const data = await buildPayload({ request, clientId })

	const payload = Object.entries(data)
		.map(([key, value]) => value && `${key}=${encodeURI(String(value))}`)
		.join('&')

	if (!IS_PROD) return

	await fetch('https://www.google-analytics.com/collect?' + payload)
}

export const setAnalyticsCookie = (
	response: NextResponse,
	value: string
): void => {
	const maxAge = 2 * 365 * 24 * 60 * 60 * 1000 // 2 years
	response.cookies.set(ANALYTICS_COOKIE_NAME, value, {
		sameSite: 'strict',
		httpOnly: true,
		maxAge,
	})
}

const getPageTitle = async (request: NextRequest) => {
	const { pathname } = request.nextUrl

	const isReading = pathname === '/reading'
	const isHome = pathname === '/'
	const isBlogHome = pathname === '/blog'
	const isBlogPost = pathname.includes('/blog/')

	try {
		if (isHome) return 'Home'
		if (isBlogHome) return 'Blog Home'
		if (isReading) return 'Reading list'

		return 'unrecognized'
	} catch (error) {
		if (typeof error === 'string') {
			return error.toUpperCase() // works, `e` narrowed to string
		} else if (error instanceof Error) {
			return error.message // works, `e` narrowed to Error
		}
	}
}
