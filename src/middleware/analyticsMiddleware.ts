import { v4 } from "uuid";

import { Middleware } from "middleware";
import { ANALYTICS_COOKIE_NAME, setAnalyticsCookie, sendPageview } from "src/clients/analyticsClient";

export const analyticsMiddleware: Middleware = ({request, response, event}) => {
	const { cookies } = request

	const analyticsCookie = cookies.get(ANALYTICS_COOKIE_NAME)
	const client_id = analyticsCookie?.value|| v4()

	!analyticsCookie && setAnalyticsCookie(response, client_id)

	event.waitUntil(sendPageview(request, client_id))
}