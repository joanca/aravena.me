import matter from 'gray-matter'

export type ParsedMarkdown<T> = {
	data: T,
	content: string
}

export const parseMarkdown = <T,>(markdown: string): ParsedMarkdown<T> =>  {
	return matter(markdown) as unknown as ParsedMarkdown<T>
}