import Link from 'next/link'
import { Fragment } from 'react'

import { CategoryType } from '@cms/types/wordpress'

const getCategoryLink = (category: CategoryType): JSX.Element => (
	<Link href={`/blog/category/${category.slug}`} title={category.description}>
		{`#${category.name.toLocaleLowerCase()}`}
	</Link>
)

const getCategoriesLink = (categories: CategoryType[]): JSX.Element => (
	<>
    in{' '}
		{categories.map((category, index) => {
			const isLast = index === categories.length - 1
			return (
				<Fragment key={`${category.id}`}>
					{getCategoryLink(category)}
					{!isLast && ', '}
				</Fragment>
			)
		})}
	</>
)

export { getCategoriesLink }
