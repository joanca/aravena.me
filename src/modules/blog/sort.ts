
import { Blogpost } from "@cms/types/blog"

export const sortByPublishDate = (posts: Blogpost[]): Blogpost[] => {
	return posts.sort((postOne, postTwo) => {
		const dateOne = new Date(postOne.publish_date).getTime()
		const dateTwo = new Date(postTwo.publish_date).getTime()

		return dateTwo - dateOne
	})
}