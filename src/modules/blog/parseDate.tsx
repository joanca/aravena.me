export type DateType = {
	date: string
	time: string
}

const parseDate = (dateString: string): DateType => {
	const dateObject = new Date(dateString).toLocaleString('es')
	const [date, time] = dateObject.split(', ')

	return { date: date?.trim(), time: time?.trim() }
}

export { parseDate }
