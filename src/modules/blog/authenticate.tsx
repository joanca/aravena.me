import { IncomingMessage } from 'http'

import { GetServerSidePropsContext } from 'next'
import { setCookie } from 'nookies'

import getRawBody from 'raw-body'
import { v4 } from 'uuid'

const LOGIN_URL = '/admin/login'

export const loginUrl = (req: IncomingMessage): string =>
	req.url ? `${LOGIN_URL}?redirect=${req.url}` : LOGIN_URL

export const redirectUrl = (req: IncomingMessage): string => req.url || '/admin'

export const isAuthenticated = async (
	context: GetServerSidePropsContext
): Promise<boolean> => {
	const { req } = context
	const isPostRequest = req.method === 'POST'

	if (!isPostRequest) return checkLogin(context)

	return validatePassword(context)
}

const checkLogin = (context: GetServerSidePropsContext): boolean => {
	const { req } = context
	const { authentication } = req.cookies

	if (!authentication) return false

	addAuthCookie(context)

	return true
}

const validatePassword = async (
	context: GetServerSidePropsContext
): Promise<boolean> => {
	const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD as string

	const { req } = context
	const body = await getRawBody(req, { encoding: true })
	const inputPassword = body.split('=')[1]

	const isValid = ADMIN_PASSWORD === inputPassword

	if (!isValid) return false

	addAuthCookie(context)
	return true
}

const addAuthCookie = (context: GetServerSidePropsContext): void => {
	setCookie(context, 'authentication', v4(), {
		maxAge: 12 * 60 * 60,
		httpOnly: true,
		sameSite: 'strict',
	})
}
