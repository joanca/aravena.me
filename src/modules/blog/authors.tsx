import Link from 'next/link'

export const AUTHOR_NAME = new Map([[197563454, 'acsacsar']])

const getAuthorLink = (id: number): JSX.Element => {
	const name = AUTHOR_NAME.get(id) || ''

	return (
		<Link
			href={`https://twitter.com/${name}`}
			title={`Twitter del autor ${name}`}
			passHref
		>
			{name}
		</Link>
	)
}

const getAuthorName = (id: number): JSX.Element => {
	const name = AUTHOR_NAME.get(id) || ''

	return <strong>{name}</strong>
}

export { getAuthorLink, getAuthorName }
