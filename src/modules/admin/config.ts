import {
	type CmsBackend,
	type CmsCollection,
	type InitOptions,
} from 'decap-cms-core'

type CustomConfig = {
	collections?: CmsCollection[]
}

export const getDecapCMSConfig = ({
	collections,
}: CustomConfig = {}): InitOptions => {
	const baseCollections = getCollections()
	const mergedCollections = collections
		? [...baseCollections, ...collections]
		: baseCollections

	return {
		config: {
			backend: getBackend(),
			collections: mergedCollections,
			media_folder: 'public/static/uploads',
			slug: {
				encoding: 'ascii',
				clean_accents: true,
			},
		},
	}
}

const getBackend = (): CmsBackend => {
	return {
		name: 'gitlab',
		repo: 'joanca/aravena.me', // Path to your GitLab repository
		auth_type: 'pkce', // Required for pkce
		app_id: 'b597402c6bdba4719266ee148d071e27e427230f1abd3dcdaa1f6dc68a22255c', // Application ID from your GitLab settings
		commit_messages: {
			create: '[create] [{{collection}}] "{{slug}}"',
			update: '[update] [{{collection}}] "{{slug}}"',
			delete: '[delete] [{{collection}}] "{{slug}}"',
			uploadMedia: '[upload] "{{path}}"',
			deleteMedia: '[delete] "{{path}}"',
			openAuthoring: '{{message}}',
		},
	}
}

const getCollections = (): CmsCollection[] => {
	return [blogCollection, customCollection]
}

const customCollection: CmsCollection = {
	name: 'custom-collection',
	label: 'Custom Collection',
	folder: 'config/collections',
	create: true,
	slug: '{{fields.slug}}',
	fields: [
		{
			label: 'Slug',
			name: 'slug',
			required: true,
			widget: 'string',
		},

		{
			label: 'Title',
			name: 'title',
			required: true,
			widget: 'string',
		},

		{
			label: 'Collection',
			name: 'body',
			required: true,
			widget: 'string',
		},
	],
}

const blogCollection: CmsCollection = {
	name: 'blog',
	label: 'Blog',
	folder: 'content/blog',
	path: '{{slug}}/index',
	media_folder: '',
	public_folder: '',
	create: true,
	slug: '{{fields.slug}}',
	fields: [
		{
			label: 'Status',
			name: 'status',
			widget: 'select',
			required: true,
			options: [
				{ label: 'Published', value: 'published' },
				{ label: 'Draft', value: 'draft' },
			],
			default: 'draft',
		},
		{ label: 'Title', name: 'title', widget: 'string', required: true },
		{ label: 'Slug', name: 'slug', widget: 'string', required: true },
		{
			label: 'Publish Date',
			name: 'publish_date',
			widget: 'datetime',
			required: true,
		},
		{
			label: 'Featured Image',
			name: 'featured_image',
			widget: 'image',
			required: false,
		},
		{
			label: 'Body',
			name: 'body',
			widget: 'markdown',
			required: true,
		},
	],
}
