import { DOMAIN } from 'src/env'

const DEFAULT_QUALITY = 75

type SizeProps = {
	width: number
	height: number
}

type ImageQuality = {
	quality: number
}

type ResizerProps = SizeProps &
ImageQuality & {
	proxy: boolean
}

type ImageResizerProps = (
	src: string,
	options?: Partial<ResizerProps>
) => string | null

export const imageLoader: ImageResizerProps = (src, options) => {
	const isExternalImage = src.startsWith('http')

	const imageResizerUrl = isExternalImage
		? getExternalImageUrl(src)
		: getImageUrl(src)

	if (!options) return imageResizerUrl

	const srcDelimiter = isExternalImage ? '&' : '?'

	return `${imageResizerUrl}${srcDelimiter}${paramsUrlPath(options, '&')}`
}

const getImageUrl = (src: string): string => `${DOMAIN}/img/${src}`

const getExternalImageUrl = (src: string): string => {
	const imageUrl = new URL(src)

	const absoluteUrl = `${imageUrl.origin}${imageUrl.pathname}`

	const encodedUrl = encodeURIComponent(absoluteUrl)
	const imageResizerUrl = `/img?src=${encodedUrl}`

	return imageResizerUrl
}

const paramsUrlPath = (
	options: Partial<ResizerProps>,
	separator: string
): string => {
	let path = ''
	let hasQualityParam = false

	Object.entries(options).forEach(([key, value]) => {
		if (key === 'q') hasQualityParam = true
		path += `${key[0]}=${value as number}${separator}`
	})

	if (!hasQualityParam) return `${path.slice(0, -1)}&q=${DEFAULT_QUALITY}`

	return path.slice(0, -1) // removes last separator
}
