export function encodeToBase64(str: string) {
	// Convert the string to a Uint8Array
	const utf8 = new TextEncoder().encode(str)

	// Create a base64 encoded string
	const base64String = btoa(String.fromCharCode(...utf8))

	return base64String
}

export function decodeFromBase64(base64String: string) {
	// Convert the base64 string to a Uint8Array
	const binaryString = atob(base64String)
	const bytes = new Uint8Array(binaryString.length)
	for (let i = 0; i < binaryString.length; i++) {
		bytes[i] = binaryString.charCodeAt(i)
	}

	// Decode the Uint8Array to a string
	const str = new TextDecoder().decode(bytes)

	return str
}
