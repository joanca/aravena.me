import { Fragment } from 'react'
import * as runtime from 'react/jsx-runtime'

import { compile, run, runSync, RunOptions } from '@mdx-js/mdx'
import remarkGfm from 'remark-gfm'

type MDXContent = ReturnType<typeof runSync>['default']
type MDXRuntime = Omit<RunOptions, 'baseUrl' | 'Fragment'>

export const getMDXfromContent = async (
	content: string
): Promise<MDXContent> => {
	const mdxContent = await compile(content, {
		outputFormat: 'function-body',
		development: false,
		remarkPlugins: [remarkGfm],
	})

	const { default: MDX } = await run(mdxContent, {
		...(runtime as MDXRuntime),
		baseUrl: import.meta.url,
		Fragment,
	})

	return MDX
}
