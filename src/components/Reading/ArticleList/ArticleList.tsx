import React, { FC } from 'react'

import { getSortedArticles } from 'src/clients/getPocket'

import { Article } from '@components/Reading/Article';
import { FeaturedArticle } from '@components/Reading/Article';

import { FeaturedArticleProps } from '@components/Reading/Article/FeaturedArticle';
import { ArticleProps } from '@components/Reading/Article/Article';

import styles from './ArticleList.module.scss'

export const ArticleList: FC = async () => {
	const articles = await getSortedArticles();

	const getPriority = (index: number) => {
		return [1,2,3].includes(index)
	}

	const featuredArticle = articles.find(article => article.image && article.readTime) as FeaturedArticleProps
	const otherArticles = articles.filter(article => {
		const notFeatured = article.id !== featuredArticle.id
		const validArticle = article.title && article.image && article.excerpt

		return notFeatured && validArticle
	}) as ArticleProps[]

	return (
		<section className={styles.container}>
			<FeaturedArticle {...featuredArticle} />

			<div className={styles.articleCards}>
				{otherArticles.slice(0, 9).map((article, index) => (
					<Article key={article.id} {...article} priority={getPriority(index)} />
				))}
			</div>
		</section>
	)
}
