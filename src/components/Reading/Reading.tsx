import { FC } from "react";

import { Header } from "@components/Reading/Header";
import { ArticleList } from "@components/Reading/ArticleList";

import styles from './Reading.module.scss'

export const Reading: FC = () => {

	return (
		<div className={styles.container}>
			<Header />
			<ArticleList />
		</div>
	)
}
