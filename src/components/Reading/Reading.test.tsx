import { act, render, screen, within } from '@testing-library/react'

import { Reading } from './Reading'

jest.mock('next/navigation', () => ({
	useRouter: () => ({
		pathname: '/reading',
	}),
}))

describe.skip('Reading Page', () => {
	beforeEach(async () => {
		const component = await Reading({})
		await act(() => render(component))
	})

	test('should get the header text before articles', () => {
		const { getByText } = screen

		const title = getByText(/reading list/i)
		const pocketDescription = getByText(/this list shows the last 20 articles/i)
		const topicsDescription = getByText(/most of the articles are about/i)

		// eslint-disable-next-line max-len
		const expectedPocketDescription =
      "This list shows the last 20 articles that I saved to my Pocket account and that I haven't read yet, it updates automatically when I archive them."

		// eslint-disable-next-line max-len
		const expectedTopicsDescription =
      'Most of the articles are about science, politics and a bit of philosophy, have fun!'

		expect(title).toBeVisible()
		expect(title).toHaveTextContent('Reading List')

		expect(pocketDescription).toBeVisible()
		expect(pocketDescription).toHaveTextContent(expectedPocketDescription)

		expect(topicsDescription).toBeVisible()
		expect(topicsDescription).toHaveTextContent(expectedTopicsDescription)
	})

	test.only('should get the articles information', () => {
		const { getAllByRole, debug } = screen

		debug()

		const articles = getAllByRole('article')

		expect(articles.length).toBe(4)

		testFirstArticle(articles[0])
		testSecondArticle(articles[1])
		testThirdArticle(articles[2])
		testFourthArticle(articles[3])
	})
})

const testFirstArticle = (article: HTMLElement) => {
	const { getByText } = within(article)

	const title = getByText(/how political opinions change/i)
	const readingTime = getByText(/5 minutes/i)
	const excerpt = getByText(/our political opinions and attitudes/i)

	expect(title).toBeVisible()
	expect(title).toHaveTextContent('How Political Opinions Change')

	expect(readingTime).toBeVisible()
	expect(readingTime).toHaveTextContent('5 minutes')

	expect(excerpt).toBeVisible()
	// eslint-disable-next-line max-len
	expect(excerpt).toHaveTextContent(
		'Our political opinions and attitudes are an important part of who we are and how we construct our identities. Hence, if I ask your opinion on health care, you will not only share it with me, but you will likely resist any of my attempts to persuade you of another point of view.'
	)
}

const testSecondArticle = (article: HTMLElement) => {
	const { getByText } = within(article)

	const title = getByText(/columna de mario vargas llosa/i)
	const readingTime = getByText(/6 minutes/i)
	const excerpt = getByText(/El caso de Colombia es muy curioso./i)

	expect(title).toBeVisible()
	expect(title).toHaveTextContent(
		'Columna de Mario Vargas Llosa: El ejemplo colombiano'
	)

	expect(readingTime).toBeVisible()
	expect(readingTime).toHaveTextContent('6 minutes')

	expect(excerpt).toBeVisible()
	// eslint-disable-next-line max-len
	expect(excerpt).toHaveTextContent(
		'El caso de Colombia es muy curioso. Ningún país latinoamericano ha padecido tantas guerras civiles y, sin embargo, con la misma seguridad puede decirse que ningún otro ha sido más libre, civil y democrático en ese mismo período.'
	)
}

const testThirdArticle = (article: HTMLElement) => {
	const { getByText, queryByText } = within(article)

	const title = getByText(/when engineers were humanists/i)
	const readingTime = queryByText(/5 minutes/i)
	const excerpt = getByText(/jessica riskin teaches history at stanford/i)

	expect(title).toBeVisible()
	expect(title).toHaveTextContent('When Engineers Were Humanists')

	expect(readingTime).toBeNull()

	expect(excerpt).toBeVisible()
	// eslint-disable-next-line max-len
	expect(excerpt).toHaveTextContent(
		'Jessica Riskin teaches History at Stanford. Her latest book is The Restless Clock: A History of the Centuries-Long Argument Over What Makes Living Things Tick. (March 2021)'
	)
}

const testFourthArticle = (article: HTMLElement) => {
	const { getByText, queryByText } = within(article)

	const title = getByText(/presidente Piñera defiende/i)
	const readingTime = queryByText(/5 minutes/i)
	const excerpt = getByText(/10991/i)

	expect(title).toBeVisible()
	expect(title).toHaveTextContent(
		'Presidente Piñera defiende control de identidad preventivo'
	)

	expect(readingTime).toBeNull()

	expect(excerpt).toBeVisible()
	// eslint-disable-next-line max-len
	expect(excerpt).toHaveTextContent('10991')
}
