import { FC } from "react";

import { Article  } from 'src/clients/types'
import { Image  } from '@components/Blog/Shared/Article/Content/Media/Image'

import { NonNullableFields } from "@components/types";

import styles from './FeaturedArticle.module.scss'

export type FeaturedArticleProps = NonNullableFields<Article>

export const FeaturedArticle: FC<FeaturedArticleProps> = (props) => {
	const {
		id,
		title,
		url,
		excerpt,
		readTime,
		image,
	} = props

	return (
		<article className={styles.featuredArticle}>
			<a href={url} title={title} target="_blank" rel="noreferrer">
				<div className={styles.image}>
					<Image
						id={id}
						src={image}
						alt={title}
						showCaption={false}
						priority
						fill
					/>
				</div>

				<div className={styles.content}>
					<h2>{title}</h2>

					<span>{`${readTime} minutes`}</span>

					<p className={styles.excerpt}>{excerpt}</p>
				</div>
			</a>
		</article>
	)
}
