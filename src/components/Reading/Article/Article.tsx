import { FC } from "react";

import { Article as ArticleType } from 'src/clients/types'
import { Image  } from '@components/Blog/Shared/Article/Content/Media/Image'

import { NonNullableField } from "@components/types";

import styles from './Article.module.scss'

export type ArticleProps = NonNullableField<ArticleType, 'image' > & {
	priority: boolean
}

export const Article: FC<ArticleProps> = (props) => {
	const {
		id,
		title,
		url,
		excerpt,
		readTime,
		image,
		priority
	} = props

	return (
		<article className={styles.article}>
			<a href={url} title={title} target="_blank" rel="noreferrer">
				<header className={styles.header}>
					<Image
						id={id}
						src={image}
						alt={title}
						size={{
							width: 400,
							height: 160,
						}}
						showCaption={false}
						priority={priority}
					/>

					<h3>{title}</h3>
				</header>

				<section className={styles.excerpt}>
					<p>{excerpt}</p>
				</section>
			</a>
		</article>
	)
}
