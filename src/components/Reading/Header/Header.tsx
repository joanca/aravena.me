import { FC } from "react";

import styles from './Header.module.scss'

export const Header: FC = () => {
	return (
		<header className={styles.header}>
    	<h1>What I'm reading</h1>

			<p>
				This is a dynamic list that shows the latest articles that I saved on my reading list.
			</p>
		</header>
	)
}
