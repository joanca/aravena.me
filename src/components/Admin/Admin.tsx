'use client'

import { type FC } from 'react'
import Script from 'next/script'
import { type InitOptions, type CMS } from 'decap-cms-core'

import { DecapCMSPreview, getPreviewCSSLinks } from './Publish/PostPreview'

import { IS_PROD } from 'src/env'

declare global {
	interface Window {
		DecapCmsApp: CMS
		CMS_MANUAL_INIT: boolean
	}
}

const REACT_URL = `https://unpkg.com/react@18/umd/react.${
	IS_PROD ? 'production.min.js' : 'development.js'
}`
const REACT_DOM_URL = `https://unpkg.com/react-dom@18/umd/react-dom.${
	IS_PROD ? 'production.min.js' : 'development.js'
}`

const DECAP_CMS_URL = `https://unpkg.com/decap-cms-app@3/dist/decap-cms-app.js`

type CMSWrapperProps = {
	cmsConfig: InitOptions
}

export const CMSWrapper: FC<CMSWrapperProps> = ({ cmsConfig }) => {
	const initDecapCMS = () => {
		window.CMS_MANUAL_INIT = true
		window.DecapCmsApp.init(cmsConfig)

		window.DecapCmsApp.registerPreviewTemplate('blog', DecapCMSPreview)

		const cssLinks = getPreviewCSSLinks()

		cssLinks.map((cssSheet) =>
			window.DecapCmsApp.registerPreviewStyle(cssSheet)
		)
	}

	return (
		<>
			<Script src={REACT_URL} strategy="beforeInteractive" />
			<Script src={REACT_DOM_URL} strategy="beforeInteractive" />
			<Script
				strategy="afterInteractive"
				src={DECAP_CMS_URL}
				onLoad={() => initDecapCMS()}
			/>
		</>
	)
}
