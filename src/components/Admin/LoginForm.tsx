import { FunctionComponent } from 'react'

const LoginForm: FunctionComponent = () => (
	<form method="POST" action="/admin">
		<p>Login form</p>
		<input type="password" name="password" placeholder="Password" />

		<button type="submit">Login</button>
	</form>
)

export { LoginForm }
