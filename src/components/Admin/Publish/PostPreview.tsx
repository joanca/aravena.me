'use client'
import { type FC, type FunctionComponent } from 'react'
import { type PreviewTemplateComponentProps } from 'decap-cms-core'

import { PostTypePublish } from '@cms/types/wordpress'
import { Article } from 'src/components/Blog/Shared/Article/Article'
import { encodeToBase64 } from 'src/modules/encodeData'
import { Blogpost } from '@cms/types/blog'

import styles from './styles.module.scss'

export type PostPreviewProps = Omit<PostTypePublish, 'status'> & {
	status: Blogpost['status']
}

const PostPreview: FunctionComponent<PostPreviewProps> = ({
	newPost,
	...props
}) => {
	const articleProps = newPost ? {} as PostPreviewProps : props

	const postId = String(newPost ? Math.random() * 1000 : (props.id ))

	const now = new Date()
	const modifiedDate = newPost ? now : new Date(articleProps.modified_at )
	const publishedDate = newPost ? now : new Date(props.publish_date )
	
	return (
		<section className={styles.preview}>
			<pre>THIS IS A PREVIEW</pre>
			<Article
				{...articleProps}
				isPreview
				id={postId}
				modified_at={modifiedDate}
				publish_date={publishedDate}
				title={articleProps.title }
				content={articleProps.content }
				excerpt={articleProps.excerpt }
			/>
		</section>
	)
}

const DecapCMSPreview: FC<PreviewTemplateComponentProps> = ({ entry }) => {
	const title = entry.getIn(['data', 'title']) as string
	const content = entry.getIn(['data', 'body']) as string
	const publish_date = entry.getIn(['data', 'publish_date']) as string
	const thumbnail = entry.getIn(['data', 'thumbnail']) as string

	const iframeUrl = '/blog/preview'
	const iframeParams = {
		content,
		publish_date,
		title,
		featured_image: thumbnail,
		status: 'draft'
	} as PostPreviewProps

	const cleanParams = removeUndefined<PostPreviewProps>(iframeParams)

	const base64Data = encodeToBase64(JSON.stringify(cleanParams))

	const iframeSrc = `${iframeUrl}?data=${base64Data}`

	// we need to use the CDN version of react for Decap CMS to work
	window.React.useEffect(() => {
		console.info('PostPreview is loading')

		const iframe = getBaseIframeDoc()

		iframe?.body.appendChild(createIframe(iframeSrc))

		console.info('PostPreview loaded correctly')
	}, [])

	window.React.useEffect(() => {
		const baseIframe = getBaseIframeDoc()

		const previewIframe = baseIframe?.getElementById(
			'decap-custom-preview'
		) as HTMLIFrameElement

		if (!previewIframe) return
		updateIframe(previewIframe, iframeSrc)
	}, [iframeSrc])

	return null
}

DecapCMSPreview.displayName = `DecapCMSPreview`

export { PostPreview, DecapCMSPreview }

const createIframe = (iframeSrc: string) => {
	const previewIframe = document.createElement('iframe')

	previewIframe.setAttribute('id', 'decap-custom-preview')
	previewIframe.setAttribute('src', iframeSrc)
	previewIframe.setAttribute('width', '100%')
	previewIframe.setAttribute('height', '100%')
	previewIframe.setAttribute('class', styles.iframePreview)

	return previewIframe
}

const updateIframe = (iframe: HTMLIFrameElement, iframeSrc: string) => {
	iframe.src = iframeSrc
}

const getBaseIframeDoc = () => {
	const iframe = document.getElementById('preview-pane') as HTMLIFrameElement
	const innerDoc = iframe.contentDocument || iframe.contentWindow?.document

	return innerDoc
}

type RemoveUndefined = <
	T extends { [prop: string]: unknown } = Record<string, never>
>(
	obj: T
) => T

const removeUndefined: RemoveUndefined = (obj) => {
	const copyObj = structuredClone(obj)

	Object.keys(copyObj).forEach(
		(key) => copyObj[key] === undefined && delete copyObj[key]
	)

	return copyObj
}

export const getPreviewCSSLinks = (): string[] => {
	const links = Array.from(document.getElementsByTagName('link'))
	return links
		.filter((link) => link.rel === 'stylesheet')
		.map((link) => link.href)
}
