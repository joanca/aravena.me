import React, { FC } from 'react'

import { Article } from '../Shared/Article/Article'

import { fetchBlogLastPosts } from '@cms/fetchers/blogFetcher'

type Props = {
	showDraft: boolean
}

const Home: FC<Props> = async ({ showDraft }) => {
	const latestPosts = await fetchBlogLastPosts(showDraft)

	return latestPosts ? (
		<>
			{latestPosts.map((post) => (
				<Article
					key={`${post.id}-${post.publish_date.toDateString()}`}
					{...post}
					isHome
				/>
			))}

			{/* <ArticlePagination {...paginationProps} /> */}
		</>
	) : null
}

export { Home }
