import { FunctionComponent } from 'react'

import {
	Image,
	ImageProps,
} from 'src/components/Blog/Shared/Article/Content/Media/Image'

import styles from './Home.module.scss'

const Thumbnail: FunctionComponent<ImageProps> = ({ src, alt, id }) => (
	<div className={styles.thumbnail}>
		<Image
			id={id}
			src={src}
			alt={alt}
			size={{
				width: 600,
				height: 280,
			}}
			showCaption={false}
		/>
	</div>
)

export { Thumbnail }
