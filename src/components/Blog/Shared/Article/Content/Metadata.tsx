import { FunctionComponent } from 'react'

import { parseDate } from 'src/modules/blog/parseDate'

import styles from '../Article.module.scss'

export type MetadataProps = {
	date: string
}

const Metadata: FunctionComponent<MetadataProps> = ({
	date
}) => {
	const parsedDate = parseDate(date)
	return (
		<div className={styles.meta}>
      Published on <time dateTime={date}>{parsedDate.date}</time>
		</div>
	)
}

export { Metadata }
