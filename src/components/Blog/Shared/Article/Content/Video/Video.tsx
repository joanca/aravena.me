
import { Youtube, YoutubeVideoProps } from './Youtube'

export const YOUTUBE = 'youtube'

type VideoProps = YoutubeVideoProps

const Video = (props: VideoProps) => {
	const { type } = props

	if (type === YOUTUBE) return <Youtube {...props} />

	return null
}

export { Video }
