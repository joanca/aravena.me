import { FunctionComponent, ReactNode } from 'react'

// @ts-ignore no types for this thing
import Lowlight from 'react-lowlight'
import 'react-lowlight/common'

import styles from './styles.module.scss'
import 'highlight.js/styles/nord.css'

type CodeType = {
	children?: ReactNode
	className?: string
}

export const Code: FunctionComponent<CodeType> = ({ children, className }) => {
	const language = className?.split('-')[1].toLocaleLowerCase()
	const code = children?.toLocaleString().trim()

	const isInline = !language

	const codeClassName = isInline ? styles.inlineCode : styles.code

	return (
		<Lowlight
			className={`${codeClassName} ${className}`}
			language={language}
			value={code}
			inline={isInline}
		/>
	)
}
