'use client'
import NextImage, { ImageLoader, ImageLoaderProps } from 'next/image'
import React, { CSSProperties } from 'react'

import { imageLoader } from 'src/modules/imageResizer'

const DEFAULT_WIDTH = 1000
const DEFAULT_HEIGHT = 450

export type ImageSizeType = {
	width: number
	height: number
	original?: {
		width: number
		height: number
	}
}

export type ImageProps = {
	id: number
	src: string
	alt: string
	showCaption: boolean
	size?: ImageSizeType
	priority?: boolean
	fill?: boolean
}

const customLoader: ImageLoader = ({ src, width }: ImageLoaderProps) =>
	imageLoader(src, {
		width,
	}) as string

const Image = (props: ImageProps) => {
	const {
		src,
		alt,
		size,
		showCaption = true,
		priority = false,
		fill = false,
	} = props

	const styles: CSSProperties = {
		objectFit: 'cover',
	}

	const getImageProps = () => {
		const width =
      (size && (size.original ? size.original.width : size.width)) ||
      DEFAULT_WIDTH
		const height =
      (size && (size.original ? size.original.height : size.height)) ||
      DEFAULT_HEIGHT

		return {
			style: fill ? styles : undefined,
			width: !fill ? width : undefined,
			height: !fill ? height : undefined,
		}
	}

	return (
		<figure>
			<NextImage
				loader={customLoader}
				src={src}
				alt={alt}
				priority={priority}
				fill={fill}
				{...getImageProps()}
			/>
			{showCaption && (
				<figcaption>
					<span dangerouslySetInnerHTML={{ __html: alt }} />
				</figcaption>
			)}
		</figure>
	)
}

export { Image }
