import NextLink from 'next/link'
import {
	AnchorHTMLAttributes,
	PropsWithChildren,
} from 'react'

type LinkProps = AnchorHTMLAttributes<HTMLAnchorElement>

const Link = ({ children, ...props }: PropsWithChildren<LinkProps>) => (
	<NextLink passHref href={props.href as string} {...props}>
		{children}
	</NextLink>
)

export { Link }
