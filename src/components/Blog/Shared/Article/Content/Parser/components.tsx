import { UseMdxComponents } from '@mdx-js/mdx'

import { Image } from 'src/components/Blog/Shared/Article/Content/Media/Image'
import { Video } from 'src/components/Blog/Shared/Article/Content/Video/Video'
import { Link } from 'src/components/Blog/Shared/Article/Content/Media/Link'
import { Code } from 'src/components/Blog/Shared/Article/Content/Media/Code'

export type MDXComponents = ReturnType<UseMdxComponents>

export const DEFAULT_EDITOR_COMPONENTS: MDXComponents = {
	Video,
	Image,
	// @ts-ignore
	a: Link,
	// @ts-ignore
	code: Code,
}
