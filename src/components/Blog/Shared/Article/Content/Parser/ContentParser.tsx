import { FunctionComponent } from 'react'

import { DEFAULT_EDITOR_COMPONENTS, MDXComponents } from 'src/components/Blog/Shared/Article/Content/Parser/components'
import { getMDXfromContent } from 'src/modules/mdx/parse'

type ContentParserType = {
	content: string
	isPreview: boolean
}

const COMPONENTS_REGEX = /<\w+\s/g

const getContentComponents = (content: string, isPreview: boolean): MDXComponents => {
	const isEmptyContent = content ? content.length === 0 : false;
	if(isEmptyContent || isPreview) return DEFAULT_EDITOR_COMPONENTS; 

	const usedComponents = content.match(COMPONENTS_REGEX)?.map(match => match.trim().substring(1))

	if(!usedComponents) return DEFAULT_EDITOR_COMPONENTS;

	const uniqueComponents = Array.from(new Set(usedComponents))

	return Object.fromEntries(
		Object.entries(DEFAULT_EDITOR_COMPONENTS).filter(
		   ([key, val])=> uniqueComponents.includes(key)
		)
	);
}

const ContentParser: FunctionComponent<ContentParserType> = async ({
	content, isPreview
}) => {
	const usedComponents = getContentComponents(content, isPreview)
	const MDX = await getMDXfromContent(content)

	return <MDX components={usedComponents} />
}

export { ContentParser }
