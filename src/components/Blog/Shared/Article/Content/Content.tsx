import { FunctionComponent } from 'react'

import { ContentParser } from 'src/components/Blog/Shared/Article/Content/Parser/ContentParser'

import styles from '../Article.module.scss'

type ContentType = {
	content: string
	isPreview: boolean
}

const Content: FunctionComponent<ContentType> = ({ content, isPreview }) => {
	return (
		<div className={styles.content}>
			<ContentParser content={content} isPreview={isPreview} />
		</div>
	)
}

export { Content }
