import React, { FC, FunctionComponent } from 'react'

import Link from 'next/link'

import { Content } from './Content/Content'
import { Metadata, MetadataProps } from './Content/Metadata'


import { Blogpost } from '@cms/types/blog'

import styles from './Article.module.scss'

type ArticleProps = Blogpost & { 
	isPreview?: boolean;
	isHome?: boolean 
}

const Article: FunctionComponent<ArticleProps> = (props) => {
	const metadataProps: MetadataProps = {
		date: props.publish_date.toDateString(),
	}

	return (
		<article className={styles.container}>
			<ArticleTitle {...props} />

			<Metadata {...metadataProps} />

			<Content {...props} isPreview={Boolean(props.isPreview)} />
		</article>
	)
}

type ArticleTitleProps = Pick<ArticleProps, 'title' | 'slug' | 'isHome'>

const ArticleTitle: FC<ArticleTitleProps> = ({ title, isHome, slug }) => {
	if(!isHome) return <h1>{title}</h1>

	return (
		<h1>
			<Link href={`/blog/${slug}`} passHref>{title}</Link>
		</h1>
	)
}

export { Article }
