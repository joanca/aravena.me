import { FC, PropsWithChildren } from "react";

// eslint-disable-next-line @typescript-eslint/ban-types
export type FCC<Props = {}> = FC<PropsWithChildren<Props>> 

export type NonNullableFields<T> = {
	[P in keyof T]: NonNullable<T[P]>;
};

export type NonNullableField<T, K extends keyof T> = T &
NonNullableFields<Pick<T, K>>;
