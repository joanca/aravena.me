import { FunctionComponent  } from 'react'
import Link from 'next/link'

import { BlogIcon } from '@components/Home/social/BlogIcon'
import { ReadingIcon } from '@components/Home/social/ReadingIcon'
import { GitlabIcon } from '@components/Home/social/GitlabIcon'
import { LinkedinIcon } from '@components/Home/social/LinkedinIcon'
import { TwitterIcon } from '@components/Home/social/TwitterIcon'

import styles from './Home.module.scss'

export const Home: FunctionComponent = () => {
	return (
		<main className={styles.home}>
			<header>
				<h1>jc</h1>
			</header>
			<section>
				<Link href="/blog" passHref aria-label='blog' prefetch={false}>
					<BlogIcon />
				</Link>

				<Link href="/reading" passHref aria-label='reading list' prefetch={false}>
					<ReadingIcon />
				</Link>
				
				<a
					href="https://www.linkedin.com/in/jcaravena/"
					aria-label="LinkedIn profile"
				>
					<LinkedinIcon />
				</a>
				<a href="https://gitlab.com/joanca" aria-label="Gitlab profile">
					<GitlabIcon />
				</a>
				<a href="https://twitter.com/acsacsar" aria-label="Twitter profile">
					<TwitterIcon />
				</a>
			</section>
		</main>
	)
}
