import React, { FunctionComponent } from 'react'
import MDEditor from '@uiw/react-md-editor'

import '@uiw/react-md-editor/markdown-editor.css'

export type EditorProps = {
	value: string
	onChange: (value?: string) => void
}

export const MarkdownEditor: FunctionComponent<EditorProps> = (props) => {
	const { value, onChange } = props

	return (
		<MDEditor value={value} onChange={onChange} preview="edit" height={600} />
	)
}
