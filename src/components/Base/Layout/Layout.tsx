import { Header } from './Header/Header'
import { Footer } from './Footer'

import { FCC } from '@components/types'

import styles from './Layout.module.scss'

export const Layout: FCC = ({ children }) => {
	return (
		<main className={styles.layoutContainer}>
			<Header />
			{children}
			<Footer />
		</main>
	)
}

