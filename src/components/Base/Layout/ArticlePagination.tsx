import Link from 'next/link'
import { FunctionComponent } from 'react'

import styles from './styles.module.scss'

export type PaginationProps = {
	currentPage: number
	totalPages: number
}

const ArticlePagination: FunctionComponent<PaginationProps> = ({
	currentPage,
	totalPages,
}) => {
	const pages = [...Array(totalPages).keys()].map((n) => {
		const number = n + 1
		const currentPageStyle = number === currentPage ? 'border-t-2' : ''

		return (
			<Link key={`pagination-${number}`} href={`/blog/page/${number}`} passHref>
				<span className={`${styles.pageItem} ${currentPageStyle}`}>
					{number}
				</span>
			</Link>
		)
	})

	return (
		<div className="flex flex-col items-center my-12">
			<div className="flex text-gray-700">
				<LeftArrow previousPage={currentPage - 1} />

				<div className="flex h-8 font-medium ">{pages}</div>

				<RightArrow nextPage={currentPage + 1} />
			</div>
		</div>
	)
}

type ArrowType = {
	previousPage: number
	nextPage: number
}

const LeftArrow: FunctionComponent<Pick<ArrowType, 'previousPage'>> = ({
	previousPage,
}) => {
	const href = previousPage <= 1 ? '/blog' : `/blog/page/${previousPage}`
	return (
		<Link href={href} passHref>
			<div className="h-8 w-8 mr-1 flex justify-center items-center  cursor-pointer">
				<svg
					xmlns="http://www.w3.org/2000/svg"
					width="100%"
					height="100%"
					fill="none"
					viewBox="0 0 24 24"
					stroke="currentColor"
					strokeWidth="2"
					strokeLinecap="round"
					strokeLinejoin="round"
					className="feather feather-chevron-left w-4 h-4"
				>
					<polyline points="15 18 9 12 15 6" />
				</svg>
			</div>
		</Link>
	)
}

const RightArrow: FunctionComponent<Pick<ArrowType, 'nextPage'>> = ({
	nextPage,
}) => (
	<Link href={`/blog/page/${nextPage}`} passHref>
		<div className="h-8 w-8 ml-1 flex justify-center items-center  cursor-pointer">
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width="100%"
				height="100%"
				fill="none"
				viewBox="0 0 24 24"
				stroke="currentColor"
				strokeWidth="2"
				strokeLinecap="round"
				strokeLinejoin="round"
				className="feather feather-chevron-right w-4 h-4"
			>
				<polyline points="9 18 15 12 9 6" />
			</svg>
		</div>
	</Link>
)

export { ArticlePagination }
