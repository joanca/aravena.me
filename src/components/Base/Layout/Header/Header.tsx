import { FunctionComponent } from 'react'
import Link from 'next/link'

import styles from './Header.module.scss'

const Header: FunctionComponent = () => {
	return (
		<header className={styles.container}>
			<Link passHref href="/" prefetch={false}>/home</Link>

			<Link href="/blog" passHref prefetch={false}>/blog</Link>
			
			<Link href="/reading" passHref prefetch={false}>/reading</Link>
		</header>
	)
}

export { Header }
