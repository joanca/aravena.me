export type Blogpost = {
	id: string
	title: string
	content: string
	excerpt: string
	status: 'published' | 'draft'
	slug: string
	featured_image: string
	publish_date: Date
	modified_at: Date
}
