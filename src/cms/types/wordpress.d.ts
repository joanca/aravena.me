export type CategoryType = {
	id: number
	parentId: number
	count: number
	name: string
	description: string
	slug: string
}

export type PostType = {
	id: number
	content: string
	title: string
	status: PostStatus
	publish_date: string
	modified_at: string
	slug: string
	excerpt: string
	featured_image: string
	author: number
	categories: CategoryType[]
}

export type PageType = {
	id: number
	content: string
	title: string
	status: string
	publish_date: string
	modified_at: string
	slug: string
	excerpt: string
	featured_image: string | null
	author: number
}

export type PostStatus = 'publish' | 'draft' | 'future' | 'pending' | 'private'
export type QueryParams = {
	status: PostStatus
	per_page: number
	search: string
	exclude: number
	order: 'asc' | 'desc'
	slug: string
	categories: number
	context: 'edit'
}

export type PostTypePublish = PostType & { newPost: boolean }

export type WordPressCategoryType = {
	id: number
	parent: number
	count: number
	name: string
	description: string
	slug: string
}

export type WordPressPageType = {
	id: number
	date_gmt: string
	modified_gmt: string
	status: string
	slug: string
	author: number
	content: {
		raw: string
	}
	title: {
		raw: string
	}
	excerpt: {
		raw: string
	}
	jetpack_featured_media_url: string
}
