import {
	type CmsCollection,
} from 'decap-cms-core'

import { getRawContent, getCollectionContent } from 'src/clients/gitlabClient'
import { parseMarkdown } from 'src/modules/blog/parseMarkdown'

const COLLECTION_PATH = 'config/collections'

const fetchSingleCollection = async (slug: string): Promise<CmsCollection> => {
	const collection = await getRawContent(`${COLLECTION_PATH}/${slug}`)

	const { data, content } = parseMarkdown(collection)

	return JSON.parse(content) as CmsCollection
}

export const fetchCustomCollections = async (): Promise<CmsCollection[]> => {
	const collections = await getCollectionContent(COLLECTION_PATH)

	const collectionsFilesSlugs = collections
		.filter((entry) => entry.name.includes('.md'))
		.map((entry) => entry.path.split('/')[2])

	const collectionsContent = collectionsFilesSlugs.map((collectionSlug) =>
		fetchSingleCollection(collectionSlug)
	)

	return Promise.all(collectionsContent)
}
