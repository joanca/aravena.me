import { Blogpost } from '@cms/types/blog'
import {
	getLastCommitByPath,
	getRawContent,
	getCollectionContent,
} from 'src/clients/gitlabClient'
import { parseMarkdown } from 'src/modules/blog/parseMarkdown'
import { sortByPublishDate } from 'src/modules/blog/sort'

const COLLECTION_PATH = 'content/blog'

export const fetchBlogContent = async (slug: string): Promise<Blogpost> => {
	const [blogPost, lastCommit] = await Promise.all([
		getRawContent(`${COLLECTION_PATH}/${slug}/index.md`),
		getLastCommitByPath(`${COLLECTION_PATH}/${slug}/index.md`),
	])

	const { data, content } = parseMarkdown<Blogpost>(blogPost)

	const modified_at = lastCommit?.committed_date
		? new Date(lastCommit.committed_date)
		: new Date(data.publish_date)

	const enrichedData: Blogpost = {
		...data,
		content,
		modified_at,
	}

	return enrichedData
}

export const fetchBlogLastPosts = async (
	showDraft: boolean
): Promise<Blogpost[]> => {
	const collectionContent = await getCollectionContent(COLLECTION_PATH)

	const postsSlugList = collectionContent
		.filter((entry) => entry.name === 'index.md')
		.map((entry) => entry.path.split('/')[2])

	const enrichedPostsPromises = postsSlugList.map((slug) =>
		fetchBlogContent(slug)
	)
	const enrichedPosts = await Promise.all(enrichedPostsPromises)

	const postsToRender = showDraft
		? enrichedPosts
		: enrichedPosts.filter((post) => post.status === 'published')

	return sortByPublishDate(postsToRender)
}
