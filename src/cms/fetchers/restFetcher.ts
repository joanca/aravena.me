type FetchType = typeof fetch
type FetchArgsType = Parameters<FetchType>

export const restFetcher = <DataType>(
	...args: FetchArgsType
): Promise<DataType> => fetch(...args).then((res) => res.json() as DataType)
